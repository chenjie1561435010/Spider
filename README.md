## 爬取部分

我们目标是爬取爱奇艺下面的海贼王每一集的名字，需要动态模拟输入事件和鼠标点击事件。

```java
	public class SimulatedLoginUtil {
            public static String getnewURL(String url) {
	
			// jsoup模拟海贼王超链接的点击事件,返回新的URL
			String newURL = null;
			// System.getProperties().setProperty("proxySet", "true"); 代理协议
			// System.getProperties().setProperty("https.proxyHost",
			// "192.168.130.15"); 代理IP
			// System.getProperties().setProperty("https.proxyPort", "8848"); 代理端口
			Connection con = Jsoup.connect(url);// 获取连接
	
			con.header(
					"User-Agent",
					"Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:69.0) Gecko/20100101 Firefox/69.0");// 配置模拟浏览器
			con.header("Accept-Language", "zh-CN");
			Response rs = null;
			try {
				rs = con.execute();
				Document dom = Jsoup.parse(rs.body());// 转换为Dom树
				// 直接再Dom树里面选择(根据id或者class)
				List<Element> et = dom.select(".search-box-input");
				// 在搜索框中设值,这个中文字还要转成UTF-8格式,在字符串处理模块再转
				Element inputtext = et.get(0).attr("value", "海贼王");
				String des_keyword = inputtext.attr("value");
				newURL = StringProcessUtil.produceFornewurl(url, des_keyword); //改变URL并转码
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			...续第二次请求
		}
	}
```



使用 HashMap 保存爬取出来的信息。





## 定时任务爬取并校验缓存

后台开个单线程的定时任务线程池，负责定时爬取。

```java
		Executors.newSingleThreadScheduledExecutor(new ThreadFactory() {
            AtomicInteger number = new AtomicInteger(0);

            @Override
            public Thread newThread(Runnable r) {
                return new Thread(r, "my-spider-thread-pool-" + number.incrementAndGet());
            }
        });
```

爬出来的结果和本地缓存校验，伪代码如下：

```java
	public static boolean doCheck(Map<Integer, String> map) {
        Iterator<Map.Entry<Integer, String>> it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Integer, String> entry = it.next();
            if (!cache.containsKey(entry.getKey())) {
                //更新缓存
                cache.put(entry.getKey(), entry.getValue());
                return false;
            }
        }
        return true;
    }
```





## 邮件通知

需要先开启邮箱的 SMTP 服务。

```java
	public class SendEmailUtil {
	    public static void send() {
	        final String send_account = "cj1561435010@163.com";
	        final String send_password = ReadFileUtil.readTextFile("D:\\IDEAproject\\Spider\\pwd.txt").trim();    // 密码或者是你自己的设置的授权码
	
	        // SMTP服务器
	        String MEAIL_163_SMTP_HOST = "smtp.163.com";
	        String SMTP_163_PORT = "25";// 端口号,这个是163使用到的;
	        // 收件人
	        String receive_account = "cj1561435010@163.com";
	        Properties p = new Properties(); //创建连接对象
	        //Properties.
	        p.put("mail.transport.protocol", "smtp");  //设置邮件发送的协议
	        p.setProperty("mail.smtp.host", "smtp.163.com");   //设置发送邮件的服务器(这里用的163-SMTP服务器)
	        p.setProperty("mail.smtp.auth", "true");   //设置经过账号密码的授权
	        //设置服务器的端口号
	        p.setProperty("mail.smtp.port", "25");
	        p.setProperty("mail.smtp.socketFactory.port", "25");
	        p.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	        Session session = Session.getInstance(p, new Authenticator() {
	            // 设置认证账户信息
	            @Override
	            protected PasswordAuthentication getPasswordAuthentication() {
	                return new PasswordAuthentication(send_account, send_password);
	            }
	        });
	
	        try {
	            MimeMessage message = new MimeMessage(session);   //创建邮件对象
	            message.setFrom(new InternetAddress(send_account)); //设置发件人
	            message.setRecipients(Message.RecipientType.TO, receive_account);
	            message.setRecipients(Message.RecipientType.CC, send_account); //重点：最好写这个抄送，不然大概率会被当成垃圾邮件！！
	            message.setFrom(new InternetAddress(send_account));
	            message.setSubject("one pice!");
	            message.setText("海贼王更新了！！！");
	            message.setSentDate(new Date());
	            message.saveChanges();
	            Transport.send(message);
	        } catch (MessagingException e) {
	            e.printStackTrace();
	        }
	    }
	}
```

